#ifndef PATH_H_
#define PATH_H_

#include <math.h>
#include "micropather.h"

class Path : public micropather::Graph
{
public:
	Path(bool dostuff = false);

	static void nodeToXY(void* node, int& x, int& y );

	static void* xyToNode(int x, int y);

	float LeastCostEstimate(void* stateStart, void* stateEnd);

	void AdjacentCost( void* state, std::vector< micropather::StateCost > *adjacent );

	void  PrintStateInfo( void* state );

	int gx;
	int gy;

	int sx;
	int sy;

private:
	static int MAPX;

};

#endif /* PATH_H_ */
