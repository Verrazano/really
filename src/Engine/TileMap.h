#ifndef TILEMAP_H_
#define TILEMAP_H_

#include <SFML/Graphics.hpp>
#include <vector>

class TileMap
{
public:
	TileMap(sf::Texture* texture, sf::Vector2f size);

	bool load(std::string path);

	void load(std::vector<std::vector<int> > map);

	void draw(sf::RenderTarget& window, sf::RenderStates states = sf::RenderStates::Default);

	int getTileIndex(sf::Vector2f pos);
	void setTileIndex(sf::Vector2f pos, int index);

	void addColor(sf::Color color, int index);
	sf::Color getColor(int index);

	sf::Vector2f getSize();

	std::vector<std::vector<int> > getMap();

private:
	sf::VertexArray m_verts;
	sf::Texture* m_texture;
	std::vector<std::vector<int> > m_map;
	sf::Vector2f m_size;
	std::vector<std::pair<sf::Color, int> > m_colorToIndex;

};

#endif /* TILEMAP_H_ */
