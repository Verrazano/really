#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include "ID/ID.h"
#include <string>
#include <SFML/Graphics.hpp>

class GameObject : public ID
{
public:
	GameObject(int team, sf::Vector2f size);

	virtual ~GameObject();

	virtual std::string getName() = 0;

	virtual bool draw(const sf::FloatRect& view, sf::RenderTarget& window, sf::RenderStates states = sf::RenderStates::Default);

	void drawDebug(sf::RenderWindow& window);

	void update();

	float distanceTo(sf::Vector2f pos);

	virtual void onMessage(bool leftClick);
	virtual void onMessage(bool leftClick, sf::Vector2f pos);
	virtual void onMessage(bool leftClick, GameObject* object);

	virtual void onTick();
	virtual void onDeath();
	virtual void onDraw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default);

	virtual void onSelected();

	int team;
	bool selected;
	sf::RectangleShape sprite;
	sf::RectangleShape rect;
	int health;
	sf::CircleShape selection;
	sf::RectangleShape healthBar;
	bool isStatic;
	bool pickedstate;

};

#endif /* GAMEOBJECT_H_ */
