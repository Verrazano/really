#include "Engine.h"
#include <iostream>
#include <sstream>
#include "Game/Tree.h"
#include "Game/Stone.h"
#include "Game/Gold.h"
#include "Game/TownCenter.h"
#include "Game/Worker.h"
#include "Game/Archer.h"
#include <time.h>
#include <math.h>
#include <random>

Engine& Engine::get()
{
	static Engine engine;
	return engine;

}

bool Engine::isRunning()
{
	return m_window.isOpen();

}

Engine::Engine() :
		m_map(&m_mapTexture, sf::Vector2f(32, 32))
{

	gwidth = 854;
	gheight = 480;
	mapnamesettings = "map.png";
	isHost = true;
	ip = "localhost";
	port = 13512;
	if(!settings.read("settings.cfg"))
	{
		std::cout << "could not load settings file\n";

	}

	tickCount = 0;

	if(settings.hasItem("width"))
		gwidth = settings.getItem("width").valueAsInt();
	if(settings.hasItem("height"))
		gheight = settings.getItem("height").valueAsInt();
	if(settings.hasItem("map"))
		mapnamesettings = settings.getItem("map").getValue();
	if(settings.hasItem("host"))
		isHost = settings.getItem("host").valueAsBool();
	if(settings.hasItem("ip"))
		ip = settings.getItem("ip").getValue();
	if(settings.hasItem("port"))
		port = settings.getItem("port").valueAsInt();

	srand(time(NULL));
	pather = NULL;
	seed = rnjesus();
	rng = std::mt19937(seed);
	distro = std::uniform_int_distribution<int>(0, 100000000);
	bwood = 0;
	bgold = 0;
	bstone = 0;
	bsizex = 1;
	bsizey = 1;
	bwork = 0;
	building = "";
	m_showWorkerMenu = false;
	m_rightState = true;
	m_selecting = false;
	m_mouseState = false;
	m_team = 0;
	m_hasFocus = true;
	m_window.create(sf::VideoMode(gwidth, gheight), "really");
	m_window.setFramerateLimit(30.0f);
	m_guiView = sf::FloatRect(0, 0, gwidth, gheight);
	m_gameView = sf::FloatRect(0, 0, vwidth, vheight);
	m_window.setView(sf::View(m_guiView));
	m_debug = false;
	m_objects.reserve(1000);
	if(!m_mapTexture.loadFromFile("res/mapTexture.png"))
	{
		std::cout << "Could not load map texture: \"res/mapTexture.png\"\n";

	}

	if(!m_font.loadFromFile("res/consolas.ttf"))
	{
		std::cout << "Could not load font: \"res/consolas.ttf\"\n";

	}

	gwood = 100;
	ggold = 0;
	gstone = 0;

	m_map.addColor(sf::Color::Red, Tile::RED_SPAWN);
	m_map.addColor(sf::Color::Blue, Tile::BLUE_SPAWN);
	m_map.addColor(sf::Color(1, 142, 14), Tile::GRASS);
	m_map.addColor(sf::Color(237, 201, 175), Tile::SAND);
	m_map.addColor(sf::Color(64, 164, 223), Tile::WATER);
	m_map.addColor(sf::Color(1, 100, 10), Tile::WOOD);
	m_map.addColor(sf::Color(167, 167, 167), Tile::STONE);
	m_map.addColor(sf::Color(255, 215, 0), Tile::GOLD);

	m_miniMapRect.setSize(sf::Vector2f(200, 200));
	m_miniMapRect.setOrigin(100, 100);
	m_miniMapRect.setPosition(gwidth-100, gheight-100);
	m_miniMapRect.setScale(0.9, -0.9);

	m_selectionRect.setFillColor(sf::Color(255, 255, 0, 50));
	m_selectionRect.setOutlineColor(sf::Color::Yellow);
	m_selectionRect.setOutlineThickness(2.0f);

	m_selectedText.setFont(m_font);
	m_selectedText.setColor(sf::Color::White);
	m_selectedText.setPosition(210, gheight-45);

	stockpile = Button("Stockpile", sf::Vector2f(64, 64),
			NULL, "25x wood", m_font, sf::Vector2f(42, gheight-128+42));

	archeryrange = Button("Archery Range", sf::Vector2f(64, 64),
			NULL, "200x wood\n75x stone\n50x gold",
			m_font, sf::Vector2f(42+10+64, gheight-128+42));

	barracks = Button("Barracks", sf::Vector2f(64, 64),
			NULL, "175x wood\n100x stone\n50x gold",
			m_font, sf::Vector2f(42, gheight-64-10+42));

	buildSelector.setFillColor(sf::Color(255, 0, 0, 100));
	buildSelector.setOutlineColor(sf::Color::Red);
	buildSelector.setOutlineThickness(1.0f);

	iconsText.loadFromFile("res/icons.png");
	archerIcon.loadFromFile("res/archericon.png");


}

Engine::~Engine()
{
	clearObjects();
	if(isHost)
	{
		m_listener.close();

	}

}

bool Engine::getDebug()
{
	return m_debug;

}

void Engine::setDebug(bool debug)
{
	m_debug = debug;

}

bool Engine::hasFocus()
{
	return m_hasFocus;

}

void Engine::drawDebugInfo()
{
	std::string info;
	sf::Vector2i mouse = sf::Mouse::getPosition(m_window);
	info = "\n\nmouse: (" + toString(mouse.x) + ", " + toString(mouse.y) + ")\n";

	sf::Text t(info, m_font, 16);
	t.setColor(sf::Color::Red);
	t.move(10, 10);
	m_window.draw(t);

}

void Engine::drawGUI()
{
	sf::RectangleShape bar(sf::Vector2f(gwidth, 64));
	bar.setFillColor(sf::Color(130, 82, 1));
	bar.setPosition(0, gheight-64);
	m_window.draw(bar);
	bar.setSize(sf::Vector2f(200, 128));
	bar.setPosition(0, gheight-128);
	m_window.draw(bar);
	m_window.draw(m_selectedText);
	bar.setSize(sf::Vector2f(200, 200));
	bar.setOrigin(100, 100);
	bar.setPosition(gwidth-100, gheight-100);
	m_window.draw(bar);
	m_window.draw(m_miniMapRect);

	sf::Text res("   x" + toString(gwood) + "    x " + toString(gstone) + "       x " + toString(ggold),
			m_font, 25);
	res.setColor(sf::Color::Black);

	res.setPosition(0, 20);
	m_window.draw(res);
	sf::RectangleShape resIcon(sf::Vector2f(48, 48));
	resIcon.setTexture(&iconsText);
	resIcon.setTextureRect(sf::IntRect(64, 0, 32, 32));
	m_window.draw(resIcon);
	resIcon.move(128, 0);
	resIcon.setTextureRect(sf::IntRect(0, 0, 32, 32));
	m_window.draw(resIcon);
	resIcon.move(128, 0);
	resIcon.setTextureRect(sf::IntRect(32, 0, 32, 32));
	m_window.draw(resIcon);

	if(m_showWorkerMenu)
	{

		if(stockpile.update(m_window))
		{
			bsizex = 1;
			bsizey = 1;
			bwood = 25;
			bwork = 30*10;
			bgold = 0;
			bstone = 0;
			building = "stockpile";
			buildSelector.setSize(sf::Vector2f(bsizex*32, bsizey*32));

		}
		else if(archeryrange.update(m_window))
		{
			bsizex = 3;
			bsizey = 1;
			bwood = 200;
			bstone = 75;
			bgold = 50;
			bwork = 30*30;
			building = "archery range";
			buildSelector.setSize(sf::Vector2f(bsizex*32, bsizey*32));

		}
		else if(barracks.update(m_window))
		{
			bsizex = 3;
			bsizey = 1;
			bwood = 175;
			bstone = 100;
			bgold = 50;
			bwork = 30*30;
			building = "barracks";
			buildSelector.setSize(sf::Vector2f(bsizex*32, bsizey*32));

		}

	}

	/*sf::Vector2f msize = m_map.getSize();
	sf::RectangleShape viewIndi(sf::Vector2f((180/msize.x)*(gwidth/2), (180/msize.y)*(gheight/2)));
	//viewIndi.setOrigin(sf::Vector2f((180/msize.x)*(854/2), (180/msize.y)*(480/2)));
	viewIndi.setFillColor(sf::Color::Transparent);
	viewIndi.setOutlineColor(sf::Color::Red);
	viewIndi.setOutlineThickness(1.0f);
	viewIndi.setPosition((180/msize.x)*(m_gameView.left-m_gameView.width), (180/msize.y)*(m_gameView.top-m_gameView.height*2));
	viewIndi.move(m_miniMapRect.getPosition());
	m_window.draw(viewIndi);*/

}

bool Engine::loadMap(std::string path)
{
	clearObjects();
	TileMap nmap(&m_mapTexture, sf::Vector2f(32, 32));
	bool ret = nmap.load(path);
	if(ret)
	{
		m_map.load(path);
		std::vector<std::vector<int> > map = m_map.getMap();
		for(unsigned int i = 0; i < map.size(); i++)
		{
			for(unsigned int j = 0; j < map[0].size(); j++)
			{
				int tile = map[i][j];
				if(tile == Tile::WOOD)
				{
					addObject(new Tree(), sf::Vector2f((j*32)+16, (i*32)+8));

				}
				else if(tile == Tile::STONE)
				{
					addObject(new Stone(), sf::Vector2f((j*32)+16, (i*32)+16));

				}
				else if(tile == Tile::GOLD)
				{
					addObject(new Gold(), sf::Vector2f((j*32)+16, (i*32)+16));

				}
				else if(tile == Tile::BLUE_SPAWN)
				{
					addObject(new TownCenter(0), sf::Vector2f((j*32)+16, (i*32)+16));
					addObject(new Worker(0), sf::Vector2f((j*32)+16+64, (i*32)+16));
					addObject(new Worker(0), sf::Vector2f((j*32)+16, (i*32)+16-64));
					addObject(new Worker(0), sf::Vector2f((j*32)+16-64, (i*32)+16));
					if(getTeam() == 0)
					{
						setCenter(sf::Vector2f((j*32)+16, (i*32)+16));

					}

				}
				else if(tile == Tile::RED_SPAWN)
				{
					addObject(new TownCenter(1), sf::Vector2f((j*32)+16, (i*32)+16));
					addObject(new Worker(1), sf::Vector2f((j*32)+16+64, (i*32)+16));
					addObject(new Worker(1), sf::Vector2f((j*32)+16, (i*32)+16-64));
					addObject(new Worker(1), sf::Vector2f((j*32)+16-64, (i*32)+16));
					if(getTeam() == 1)
					{
						setCenter(sf::Vector2f((j*32)+16, (i*32)+16));

					}

				}

			}

		}

		m_miniMap.create(m_map.getSize().x, m_map.getSize().y);
		m_miniMapView.width = m_map.getSize().x;
		m_miniMapView.height = m_map.getSize().y;
		m_miniMap.setView(sf::View(m_miniMapView));
		m_miniMapRect.setTexture(&m_miniMap.getTexture());

		mappath = Path(true);
		if(pather != NULL)
		{
			delete pather;

		}

		pather = new micropather::MicroPather(&mappath, 4000);

	}
	return ret;

}

void Engine::loadMap(std::vector<std::vector<int> > m)
{
	clearObjects();
	m_map.load(m);
	std::vector<std::vector<int> > map = m;
	for(unsigned int i = 0; i < map.size(); i++)
	{
		for(unsigned int j = 0; j < map[0].size(); j++)
		{
			int tile = map[i][j];
			if(tile == Tile::WOOD)
			{
				addObject(new Tree(), sf::Vector2f((j*32)+16, (i*32)+8));

			}
			else if(tile == Tile::STONE)
			{
				addObject(new Stone(), sf::Vector2f((j*32)+16, (i*32)+16));

			}
			else if(tile == Tile::GOLD)
			{
				addObject(new Gold(), sf::Vector2f((j*32)+16, (i*32)+16));

			}
			else if(tile == Tile::BLUE_SPAWN)
			{
				addObject(new TownCenter(0), sf::Vector2f((j*32)+16, (i*32)+16));
				addObject(new Worker(0), sf::Vector2f((j*32)+16+64, (i*32)+16));
				addObject(new Worker(0), sf::Vector2f((j*32)+16, (i*32)+16-64));
				addObject(new Worker(0), sf::Vector2f((j*32)+16-64, (i*32)+16));
				if(getTeam() == 0)
				{
					setCenter(sf::Vector2f((j*32)+16, (i*32)+16));

				}

			}
			else if(tile == Tile::RED_SPAWN)
			{
				addObject(new TownCenter(1), sf::Vector2f((j*32)+16, (i*32)+16));
				addObject(new Worker(1), sf::Vector2f((j*32)+16+64, (i*32)+16));
				addObject(new Worker(1), sf::Vector2f((j*32)+16, (i*32)+16-64));
				addObject(new Worker(1), sf::Vector2f((j*32)+16-64, (i*32)+16));
				if(getTeam() == 1)
				{
					setCenter(sf::Vector2f((j*32)+16, (i*32)+16));

				}

			}

		}

	}

	m_miniMap.create(m_map.getSize().x, m_map.getSize().y);
	m_miniMapView.width = m_map.getSize().x;
	m_miniMapView.height = m_map.getSize().y;
	m_miniMap.setView(sf::View(m_miniMapView));
	m_miniMapRect.setTexture(&m_miniMap.getTexture());

	mappath = Path(true);
	if(pather != NULL)
	{
		delete pather;

	}

	pather = new micropather::MicroPather(&mappath, 4000);

}

void Engine::setTeam(int team)
{
	m_team = team;

}

int Engine::getTeam()
{
	return m_team;

}

int Engine::rand()
{
	return distro(rng);

}

std::vector<GameObject*> Engine::getObjectsInArea(sf::FloatRect rect)
{
	std::vector<GameObject*> objects;
	for(auto object : m_objects)
	{
		if(rect.intersects(object->rect.getGlobalBounds()))
		{
			objects.push_back(object);

		}

	}

	return objects;

}

std::vector<GameObject*> Engine::getObjectsWithName(std::string name)
{
	std::vector<GameObject*> objects;
	for(auto object : m_objects)
	{
		if(object->getName() == name)
		{
			objects.push_back(object);

		}

	}

	return objects;

}

std::vector<GameObject*> Engine::getObjectsInRadius(float radius, sf::Vector2f from)
{
	std::vector<GameObject*> objects;
	for(auto object : m_objects)
	{
		if(object->distanceTo(from) <= radius)
		{
			objects.push_back(object);

		}

	}

	return objects;

}

GameObject* Engine::getClosestObjectInRadiusNotOnTeam(float radius, sf::Vector2f from, int notteam)
{
	GameObject* obj = NULL;
	float d = radius;
	for(auto object : m_objects)
	{
		if(object->team != -1 && object->team != notteam)
		{
			float r = object->distanceTo(from);
			if(r < d)
			{
				d = r;
				obj = object;

			}

		}

	}

	return obj;

}

std::vector<GameObject*> Engine::getObjectsInRadiusWithName(float radius, sf::Vector2f from, std::string name)
{
	std::vector<GameObject*> objects;
	for(auto object : m_objects)
	{
		if(object->getName() == name && object->distanceTo(from) <= radius)
		{
			objects.push_back(object);

		}

	}

	return objects;

}

GameObject* Engine::getClosestObjectWithName(std::string name, sf::Vector2f from)
{
	float dist = 100000000;
	GameObject* obj = NULL;
	for(auto object : m_objects)
	{
		if(object->getName() == name)
		{
			float d = object->distanceTo(from);
			if(d < dist)
			{
				dist = d;
				obj = object;

			}

		}

	}

	return obj;

}

GameObject* Engine::getClosestObjectWithNameOnTeam(std::string name, sf::Vector2f from, int team)
{
	float dist = 1000000000;
	GameObject* obj = NULL;
	for(auto object : m_objects)
	{
		if(object->getName() == name && object->team == team)
		{
			float d = object->distanceTo(from);
			if(d < dist)
			{
				dist = d;
				obj = object;

			}

		}

	}

	return obj;

}

void Engine::clearObjects()
{
	while(!m_objects.empty())
	{
		GameObject* object = (*m_objects.begin());
		m_objects.erase(m_objects.begin());
		delete object;

	}

}

std::string Engine::addObject(GameObject* object, sf::Vector2f pos)
{
	std::string id = object->getIDString();
	object->rect.setPosition(pos);
	m_objects.push_back(object);
	return id;

}

void Engine::addNetworkObject(std::string name, sf::Vector2f pos)
{
	if(!tempcommands.empty())
	{
		Command& c = tempcommands.front();
		c.created.push_back(std::pair<std::string, sf::Vector2f>(name, pos));

	}

}

GameObject* Engine::getObject(std::string id)
{
	for(auto object : m_objects)
	{
		if(object->getIDString() == id)
		{
			return object;

		}

	}

	return NULL;

}

GameObject* Engine::getObject(sf::Vector2f pos)
{
	std::vector<GameObject*> object = getObjectsInArea(sf::FloatRect(pos.x-5, pos.y-5, 10, 10));
	if(object.size() == 0)
		return NULL;
	return object[0];

}

void Engine::remObject(std::string id)
{
	std::vector<GameObject*>::iterator it;
	for(it = m_objects.begin(); it != m_objects.end(); ++it)
	{
		GameObject* object = (*it);
		if(object->getIDString() == id)
		{
			m_objects.erase(it);
			delete object;
			return;

		}

	}

}

void Engine::showWorkerMenu()
{
	closeMenus();
	m_showWorkerMenu = true;
	building = "";

}

void Engine::closeMenus()
{
	m_showWorkerMenu = false;

}

void Engine::playSound(sf::Sound& s, sf::Vector2f pos, float base)
{
	sf::Vector2f spos;
	spos.x = m_gameView.left + m_gameView.width/2;
	spos.y = m_gameView.top + m_gameView.height/2;
	spos -= pos;
	float dist = sqrtf(powf(spos.x, 2)+powf(spos.y, 2));
	float maxd = m_gameView.width/2;
	float v = maxd-dist;
	v/=maxd;
	if(v > 1.0)
		v = 1.0;
	v*=base;
	if(v <= base*0.1)
	{
		return;

	}
	s.setVolume(v);
	s.play();

}

void Engine::setCenter(sf::Vector2f pos)
{
	m_gameView.left = pos.x - m_gameView.width/2;
	m_gameView.top = pos.y - m_gameView.height/2;

}

std::vector<std::vector<int> > Engine::getMap()
{
	return m_map.getMap();

}

std::string Engine::toStirng(int i)
{
	std::stringstream ss;
	ss << i;
	std::string str;
	ss >> str;
	return str;

}

std::string Engine::toString(float f)
{
	std::stringstream ss;
	ss << f;
	std::string str;
	ss >> str;
	return str;

}

std::vector<sf::Vector2f> Engine::solvePath(sf::Vector2f pos, sf::Vector2f pos2)
{
	std::vector<sf::Vector2f> path;
	if(pather != NULL)
	{
		float totalCost = 0;
		pather->Reset();
		mappath.gx = (int)pos2.x/32;
		mappath.gy = (int)pos2.y/32;
		mappath.sx = (int)pos.x/32;
		mappath.sy = (int)pos.y/32;

		std::vector<void*> npath;
		void* node1 = Path::xyToNode((int)pos.x/32, (int)pos.y/32 );
		void* node2 = Path::xyToNode( (int)pos2.x/32, (int)pos2.y/32 );
		int result = pather->Solve( node1,
				node2, &npath, &totalCost );

		if ( result == micropather::MicroPather::SOLVED ) {
			for(unsigned int i = 0; i < npath.size(); i++)
			{
				int x, y;
				Path::nodeToXY(npath[i], x, y);
				x*=32;
				y*=32;
				x+=16;
				y+=16;
				path.push_back(sf::Vector2f(x, y));

			}

		}

	}

	return path;

}

void Engine::drawPath(std::vector<sf::Vector2f> path)
{
	sf::VertexArray verts(sf::LinesStrip, path.size());
	for(unsigned int i = 0; i < path.size(); i++)
	{
		verts[i] = sf::Vertex(path[i], sf::Color::Red);

	}

	m_window.draw(verts);

}
