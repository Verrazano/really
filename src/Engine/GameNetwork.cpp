#include "Engine.h"
#include <iostream>

void Engine::initNetwork()
{
	if(isHost)
	{
		sf::Socket::Status status = m_listener.listen(port);
		if(status == sf::Socket::Done)
		{
			if(m_listener.accept(m_socket) == sf::TcpListener::Done)
			{
				sf::Packet packet;
				std::vector<std::vector<int> > m = getMap();
				packet << seed << (unsigned int)m[0].size() << (unsigned int)m.size();
				for(unsigned int i = 0; i < m.size(); i++)
				{
					for(unsigned int j = 0; j < m[0].size(); j++)
					{
						packet << m[i][j];

					}

				}

				m_socket.send(packet);

			}

		}

	}
	else
	{
		m_team = 1;
		if(m_socket.connect(ip, port) == sf::Socket::Done)
		{
			std::cout << "recieved packet\n";
			sf::Packet packet;
			m_socket.receive(packet);
			std::vector<std::vector<int> > m;
			unsigned int width;
			unsigned int height;
			packet >> seed >> width >> height;

			for(unsigned int i = 0; i < height; i++)
			{
				m.push_back(std::vector<int>());
				for(unsigned int j = 0; j < width; j++)
				{
					int k = 0;
					packet >> k;
					m[i].push_back(k);

				}

			}

			rng = std::mt19937(seed);
			loadMap(m);

		}

	}

}
