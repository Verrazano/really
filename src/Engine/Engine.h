#ifndef ENGINE_H_
#define ENGINE_H_

#define __cplusplus 201103L

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include "GameObject.h"
#include "TileMap.h"
#include <vector>
#include "Button.h"
#include <SFML/Audio.hpp>
#include <random>
#include "Path.h"
#include "Config/Config.h"
#include <queue>
#include "Command.h"

class Engine
{
public:
	static Engine& get();

	bool isRunning();

	void tick();
	void handleInput();

	bool getDebug();
	void setDebug(bool debug);
	bool hasFocus();
	void drawDebugInfo();
	void drawGUI();

	bool loadMap(std::string path);
	void loadMap(std::vector<std::vector<int> > m);

	void setTeam(int team);
	int getTeam();

	int rand();

	std::vector<GameObject*> getObjectsInArea(sf::FloatRect rect);
	std::vector<GameObject*> getObjectsWithName(std::string name);
	std::vector<GameObject*> getObjectsInRadius(float radius, sf::Vector2f from);
	GameObject* getClosestObjectInRadiusNotOnTeam(float radius, sf::Vector2f from, int notteam);
	std::vector<GameObject*> getObjectsInRadiusWithName(float radius, sf::Vector2f from, std::string name);
	GameObject* getClosestObjectWithName(std::string name, sf::Vector2f from);
	GameObject* getClosestObjectWithNameOnTeam(std::string name, sf::Vector2f from, int team);

	void clearObjects();
	std::string addObject(GameObject* object, sf::Vector2f pos);
	void addNetworkObject(std::string name, sf::Vector2f pos);
	GameObject* getObject(std::string id);
	GameObject* getObject(sf::Vector2f pos);
	void remObject(std::string id);

	void showWorkerMenu();
	void closeMenus();

	void playSound(sf::Sound& s, sf::Vector2f pos, float base = 100.0f);

	void setCenter(sf::Vector2f pos);
	std::vector<std::vector<int> > getMap();

	void initNetwork();

	const float scrollSpeed = 20;
	const int scrollBuffer = 10;

	typedef enum Tile
	{
		RED_SPAWN = 0,
		BLUE_SPAWN = 1,
		GRASS = 2,
		SAND = 3,
		WATER = 4,
		WOOD = 5,
		STONE = 6,
		GOLD = 7

	} Tile;

	const float vwidth = 854/1.5;
	const float vheight = 480/1.5;
	float gwidth;
	float gheight;

	static std::string toStirng(int i);
	static std::string toString(float f);

	static const sf::Keyboard::Key scrollUp = sf::Keyboard::W;
	static const sf::Keyboard::Key scrollDown = sf::Keyboard::S;
	static const sf::Keyboard::Key scrollLeft = sf::Keyboard::A;
	static const sf::Keyboard::Key scrollRight = sf::Keyboard::D;

	int gwood;
	int gstone;
	int ggold;

	sf::Texture iconsText;
	sf::Texture archerIcon;
	sf::Font m_font;
	sf::Vector2f mousePos;

	std::vector<sf::Vector2f> solvePath(sf::Vector2f pos, sf::Vector2f pos2);
	void drawPath(std::vector<sf::Vector2f> path);

	sf::RenderWindow m_window;

	std::string mapnamesettings;
	bool isHost;
	//client info
	std::string ip;
	int port;

private:
	Engine();
	~Engine();

	Config settings;

	sf::Event m_event;
	sf::FloatRect m_gameView;
	sf::FloatRect m_guiView;

	bool m_debug;
	bool m_hasFocus;

	sf::TcpSocket m_socket;
	sf::TcpListener m_listener;

	std::vector<GameObject*> m_objects;

	sf::Texture m_mapTexture;
	TileMap m_map;
	sf::RenderTexture m_miniMap;
	sf::FloatRect m_miniMapView;
	sf::RectangleShape m_miniMapRect;
	int m_team;

	sf::RectangleShape m_selectionRect;
	bool m_mouseState;
	bool m_rightState;
	bool m_selecting;
	sf::Text m_selectedText;

	std::vector<GameObject*> m_selection;

	Button stockpile;
	Button archeryrange;
	Button barracks;
	bool m_showWorkerMenu;

	std::string building;
	sf::RectangleShape buildSelector;
	int bsizex;
	int bsizey;

	int seed;

	sf::Vector2f buildPos;
	int bwood;
	int bstone;
	int bwork;
	int bgold;

	std::default_random_engine rnjesus;
	std::mt19937 rng;
	std::uniform_int_distribution<int> distro;

	Path mappath;
	micropather::MicroPather* pather;

	int tickCount;
	std::queue<Command> tempcommands;
	std::queue<Command> mycommands;
	std::queue<Command> opcommands;

};

#endif /* ENGINE_H_ */
