#ifndef BUTTON_H_
#define BUTTON_H_

#include <SFML/Graphics.hpp>

class Button
{
public:
	Button(){}//kill yourself if you try to legit use this constructor
	Button(std::string name, sf::Vector2f size,
			sf::Texture* text, std::string hover,
			sf::Font& font, sf::Vector2f pos);

	bool update(sf::RenderWindow& window, sf::RenderStates states = sf::RenderStates::Default);

	void setPosition(sf::Vector2f pos);

	sf::RectangleShape button;
	sf::RectangleShape icon;
	sf::Text name;
	sf::Text hover;
	sf::RectangleShape hoverBox;
	bool ingame;

};

#endif /* BUTTON_H_ */
