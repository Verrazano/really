#include "Engine.h"
#include <iostream>
#include "Game/Scafold.h"

void Engine::handleInput()
{
	if(!hasFocus())
	{
		tempcommands.push(Command());
		return;

	}

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		closeMenus();
		for(auto object : m_selection)
		{
			object->selected = false;

		}
		m_selection.clear();

	}

	sf::Vector2f m;
	sf::Vector2i mouse = sf::Mouse::getPosition(m_window);
	mousePos = m_window.mapPixelToCoords(mouse, sf::View(m_gameView));
	if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
	{
		if(!(mouse.y >= gheight-50 || (mouse.x <= 200 && mouse.y >= gheight-128)))
		{
			if(!m_rightState)
			{
				m_rightState = true;
				/*for(auto object : m_selection)
				{
					if(object->team == m_team)
					{
						object->pickedstate = false;
						object->onMessage(false, getObject(mousePos));
						if(!object->pickedstate)
						{
							object->onMessage(false, mousePos);

						}
						if(!object->pickedstate)
						{
							object->onMessage(false);

						}

					}

				}*/
				tempcommands.push(Command(m_selection, false, mousePos));
				return;

			}

		}

	}
	else
	{
		m_rightState = false;

	}

	if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if(!(mouse.y >= gheight-50 || (mouse.x <= 200 && mouse.y >= gheight-128)))
		{
			if(!m_mouseState)
			{
				m_mouseState = true;
				if(m_showWorkerMenu && building != "" && ggold >= bgold && gstone >= bstone && gwood >= bwood)
				{
					std::vector<std::vector<int> > map = m_map.getMap();
					for(int i = 0; i < bsizey; i++)
					{
						for(int j = 0; j < bsizex; j++)
						{
							int tile = map[i+buildPos.y][j+buildPos.x];
							GameObject* object = getObject(sf::Vector2f((j+buildPos.x)*32, (i+buildPos.y)*32));
							if(tile == WATER || object != NULL)
							{
								tempcommands.push(Command());
								return;

							}

						}

					}

					ggold -= bgold;
					gstone -= bstone;
					gwood -= bwood;
					tempcommands.push(Command(m_selection, false, mousePos));
					addNetworkObject("scafold"+building,
							sf::Vector2f((buildPos.x*32)+(bsizex*16), (buildPos.y*32)+(bsizey*16)));
					//GameObject* scaf = getObject(id);
					/*for(auto object : m_selection)
					{
						object->onMessage(false, scaf);

					}*/
					closeMenus();
					for(auto object : m_selection)
					{
						object->selected = false;

					}

					m_selection.clear();
					return;

				}

				/*for(auto object : m_selection)
				{
					if(object->team == m_team)
					{
						object->pickedstate = false;
						object->onMessage(true, getObject(mousePos));
						if(!object->pickedstate)
						{
							object->onMessage(true, mousePos);

						}
						if(!object->pickedstate)
						{
							object->onMessage(true);

						}


					}

				}*/
				tempcommands.push(Command(m_selection, true, mousePos));

				sf::FloatRect bounds = m_miniMapRect.getGlobalBounds();
				if(bounds.contains(mouse.x, mouse.y))
				{
					m_gameView.left = ((mouse.x - bounds.left)/bounds.width*m_miniMap.getSize().x) - m_gameView.left/2;
					m_gameView.top = ((mouse.y - bounds.top)/bounds.height*m_miniMap.getSize().y) - m_gameView.top/2;

				}
				else
				{
					m_selecting = true;
					m_selectionRect.setPosition(mousePos);
					m_selectionRect.setSize(sf::Vector2f(2, 2));

				}

				return;

			}

			sf::Vector2f diff = mousePos;
			diff -= m_selectionRect.getPosition();
			m_selectionRect.setSize(diff);

		}

		tempcommands.push(Command());
		return;

	}
	else
	{
		m_mouseState = false;
		if(m_selecting && !(mouse.y >= gheight-50 || (mouse.x <= 200 && mouse.y >= gheight-128)))
		{
			for(auto object : m_selection)
			{
				object->selected = false;

			}

			m_selection = getObjectsInArea(m_selectionRect.getGlobalBounds());
			GameObject* other = getObject(m_selectionRect.getPosition());
			std::vector<std::pair<std::string, int> > units;
			bool hasOther = false;
			closeMenus();
			for(auto object : m_selection)
			{
				object->selected = true;
				object->onSelected();
				if(object == other)
				{
					hasOther = true;

				}

				bool found = false;
				for(auto& pair : units)
				{
					if(pair.first == object->getName())
					{
						pair.second++;
						found = true;
						break;

					}

				}
				if(!found)
				{
					units.push_back(std::pair<std::string, int>(object->getName(), 1));

				}

			}

			if(other != NULL && !hasOther)
			{
				other->onSelected();
				other->selected = true;
				m_selection.push_back(other);
				bool found = false;
				for(auto& pair : units)
				{
					if(pair.first == other->getName())
					{
						pair.second++;
						found = true;
						break;

					}

				}
				if(!found)
				{
					units.push_back(std::pair<std::string, int>(other->getName(), 1));

				}

			}

			std::string str = "";
			for(int i = 0; i < units.size(); i++)
			{
				str += units[i].first + "(" + toString(units[i].second) + "), ";

			}
			m_selectedText.setString(str);

		}

		m_selecting = false;

	}

	bool considerKeys = true;
	if(mouse.x <= scrollBuffer && mouse.x >= 0)
	{
		m.x -= scrollSpeed/2;
		considerKeys = false;

	}
	else if(mouse.x >= m_window.getSize().x-scrollBuffer && mouse.x <= m_window.getSize().x)
	{
		m.x += scrollSpeed/2;
		considerKeys = false;

	}

	if(mouse.y <= scrollBuffer && mouse.y >= 0)
	{
		m.y -= scrollSpeed/2;
		considerKeys = false;

	}
	else if(mouse.y >= m_window.getSize().y-scrollBuffer && mouse.y <= m_window.getSize().y)
	{
		m.y += scrollSpeed/2;
		considerKeys = false;

	}


	if(considerKeys)
	{
		if(sf::Keyboard::isKeyPressed(scrollUp))
		{
			m.y -= scrollSpeed;

		}
		else if(sf::Keyboard::isKeyPressed(scrollDown))
		{
			m.y += scrollSpeed;

		}

		if(sf::Keyboard::isKeyPressed(scrollLeft))
		{
			m.x -= scrollSpeed;

		}
		else if(sf::Keyboard::isKeyPressed(scrollRight))
		{
			m.x += scrollSpeed;

		}

	}

	if(m.y + m.x >= scrollSpeed*2)
	{
		m.x /= 2;
		m.y /= 2;

	}

	m_gameView.top += m.y;
	m_gameView.left += m.x;

	tempcommands.push(Command());

}
