#include "GameObject.h"
#include <iostream>
#include <math.h>

GameObject::GameObject(int team, sf::Vector2f size) :
	team(team)
{
	pickedstate = false;
	selected = false;
	isStatic = false;
	rect.setSize(size);
	rect.setOrigin(size.x/2, size.y/2);

	sprite.setSize(size);
	sprite.setOrigin(size.x/2, size.y/2);

	rect.setFillColor(sf::Color::Transparent);
	rect.setOutlineColor(sf::Color::Red);
	rect.setOutlineThickness(1.0f);

	health = 100;

	selection.setOutlineThickness(1.5f);
	selection.setFillColor(sf::Color::Transparent);
	selection.setRadius(size.x+5);
	selection.setOrigin((size.x+5), 0);
	selection.setScale(1.0, 0.3);
	if(team == -1) //neutral
		selection.setOutlineColor(sf::Color(80, 80, 80));
	else if(team == 0)
		selection.setOutlineColor(sf::Color::Blue);
	else if(team == 1)
		selection.setOutlineColor(sf::Color::Red);

	healthBar.setFillColor(sf::Color::Green);
	healthBar.setOutlineColor(sf::Color::Black);
	healthBar.setOutlineThickness(1.0f);

}

GameObject::~GameObject()
{

}

bool GameObject::draw(const sf::FloatRect& view, sf::RenderTarget& window, sf::RenderStates states)
{
	if(view.intersects(sprite.getGlobalBounds()))
	{
		if(selected)
		{
			selection.setPosition(rect.getPosition());
			window.draw(selection);

		}

		window.draw(sprite, states);

		if(selected)
		{
			healthBar.setSize(sf::Vector2f(health*0.1, 2));
			healthBar.setOrigin((float)health*0.1/2.0f, 5);
			healthBar.setPosition(sprite.getPosition());
			healthBar.move(0, -(rect.getSize().y/2)-10);
			window.draw(healthBar);

		}

		return true;

	}

	return false;

}

void GameObject::drawDebug(sf::RenderWindow& window)
{
	window.draw(rect);

}

void GameObject::update()
{
	sprite.setPosition(rect.getPosition());

}

float GameObject::distanceTo(sf::Vector2f pos)
{
	sf::Vector2f diff = pos;
	diff -= rect.getPosition();
	return sqrtf(powf(diff.x, 2) + powf(diff.y, 2));

}

void GameObject::onMessage(bool leftClick)
{
}

void GameObject::onMessage(bool leftClick, sf::Vector2f pos)
{
}

void GameObject::onMessage(bool leftClick, GameObject* object)
{
}

void GameObject::onTick()
{
}

void GameObject::onDeath()
{
}

void GameObject::onDraw(sf::RenderTarget& target, sf::RenderStates states)
{
}

void GameObject::onSelected()
{
}

