#include "Engine.h"
#include <iostream>

void Engine::tick()
{
	while(m_window.pollEvent(m_event))
	{
		if(m_event.type == sf::Event::Closed)
		{
			m_window.close();

		}
		else if(m_event.type == sf::Event::GainedFocus)
		{
			m_hasFocus = true;

		}
		else if(m_event.type == sf::Event::LostFocus)
		{
			m_hasFocus = false;

		}

	}

	handleInput();

	if(isHost)
	{
		if(!mycommands.empty())
		{
			Command& command = mycommands.front();
			command.execute();
			mycommands.pop();

		}

		if(!opcommands.empty())
		{
			Command& command = opcommands.front();
			command.execute();
			opcommands.pop();

		}

	}
	else //WE SWAP THE ORDER THAT COMMANDS HAPPEN SO THEY HAPPEN THE SAME ORDER BOTH SIDES
	{
		if(!opcommands.empty())
		{
			Command& command = opcommands.front();
			command.execute();
			opcommands.pop();

		}

		if(!mycommands.empty())
		{
			Command& command = mycommands.front();
			command.execute();
			mycommands.pop();

		}

	}


	m_window.setView(sf::View(m_gameView));
	m_window.clear(sf::Color(25, 25, 25));

	m_map.draw(m_window);
	m_map.draw(m_miniMap);

	std::vector<GameObject*>::iterator it;
	for(it = m_objects.begin(); it != m_objects.end(); ++it)
	{
		GameObject* object = (*it);
		if(object->health <= 0)
		{

			m_objects.erase(it);
			--it;
			object->onDeath();
			continue;

		}

		object->onTick();
		object->update();

		if(!object->isStatic)
		{
			sf::Vector2f objPos = object->rect.getPosition();

			for(auto obj2 : m_objects)
			{
				if(obj2 == object)
					continue;

				sf::FloatRect intersection;
				if(obj2->rect.getGlobalBounds().intersects(object->rect.getGlobalBounds(), intersection))
				{
					if(intersection.width > intersection.height)
						if(objPos.y > intersection.top)
							object->rect.move(0, intersection.height);
						else
							object->rect.move(0, -intersection.height);
					else
						if(objPos.x > intersection.left)
							object->rect.move(intersection.width, 0);
						else
							object->rect.move(-intersection.width, 0);

				}

			}

		}

		object->draw(m_miniMapView, m_miniMap);
		if(object->draw(m_gameView, m_window))
		{
			object->onDraw(m_window);
			if(m_debug)
				object->drawDebug(m_window);

		}

	}

	if(m_selecting)
	{
		m_window.draw(m_selectionRect);

	}

	if(m_showWorkerMenu && building != "")
	{
		sf::Vector2f pos = mousePos;
		pos.x /= 32;
		pos.y /= 32;
		pos.x = (int)pos.x;
		pos.y = (int)pos.y;
		buildPos = pos;
		pos.x *= 32;
		pos.y *= 32;

		buildSelector.setPosition(pos);
		m_window.draw(buildSelector);

	}

	m_window.setView(sf::View(m_guiView));

	drawGUI();

	if(m_debug)
	{
		drawDebugInfo();

	}

	m_window.display();
	tickCount++;
	if(tickCount != 0 && tickCount%7 == 0)
	{
		sf::Packet p;
		p << (unsigned int)tempcommands.size();
		while(!tempcommands.empty())
		{
			Command& command = tempcommands.front();
			mycommands.push(command);
			command.addTo(p);
			tempcommands.pop();

		}
		m_socket.send(p);
		p.clear();
		m_socket.receive(p);
		unsigned int num = 0;
		p >> num;
		for(unsigned int i = 0; i < num; i++)
		{
			Command command;
			command.recieve(p);
			opcommands.push(command);

		}

	}

}
