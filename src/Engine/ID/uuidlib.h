/* Version 4 UUID library */

#ifndef _UUIDLIB_H
#define _UUIDLIB_H

#include <stdint.h>

/* The UUID data type */
typedef struct uuid_t {
  uint64_t n0;
  uint64_t n1;
} uuid_t;

/* Generates and returns a new UUID. The random number
   generator should be seeded with srand() before calling
   this function. */
uuid_t uuidMake();

/* Returns a nonzero value if the specified string contains
   only valid hexidecimal characters. Otherwise, a value of
   zero is returned. */
int uuid_isxstr(char *str);

/* Internal function
   Reads a segment of /count/ length at an offset of /offset/
   from /buf/, converts it to a uint64_t, and stores it
   into /num/. This function returns zero upon success, or
   -1 upon failure. */
int uuidGetStrSegment(const char *str, int offset, int count,
  uint64_t *num);

/* Decodes a string containing a UUID in the standard format
   to a uuid_t. Upon successful completion, a value of
   zero is returned. Otherwise, -1 is returned. */
int uuidFromStr(uuid_t *uuid, const char *str);

/* Encodes a uuid_t to a string in the standard format. At
   most size characters of the string will be written to
   str . If successful, the function will return the number
   of characters written to str. A negative value will
   be returned upon error. */
int uuidToStr(uuid_t uuid, char *str, int size);

/* Tests the equality of two uuid_t s. Returns one if
   first and second are equal, and zero in all other
   cases. */
int uuidIsEqual(uuid_t first, uuid_t second);

#endif /* _UUIDLIB_H */
