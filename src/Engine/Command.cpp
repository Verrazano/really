#include "Command.h"
#include "Engine.h"
#include "Game/Archer.h"
#include "Game/ArcheryRange.h"
#include "Game/Barracks.h"
#include "Game/Knight.h"
#include "Game/Scafold.h"
#include "Game/Stockpile.h"
#include "Game/Worker.h"
#include <iostream>

Command::Command()
{
	team = Engine::get().getTeam();
	leftClick = false;

}

Command::Command(std::vector<GameObject*> objs, bool leftClick, sf::Vector2f pos)
{
	this->objs = objs;
	this->pos = pos;
	this->leftClick = leftClick;
	team = Engine::get().getTeam();

}

void Command::recieve(sf::Packet& packet)
{
	objs.clear();
	created.clear();
	Engine& engine = Engine::get();
	unsigned int num = 0;
	unsigned int cnum = 0;
	packet >> team;
	packet >> cnum;
	for(unsigned int i = 0; i < cnum; i++)
	{
		std::string name;
		sf::Vector2f pos;
		packet >> name >> pos.x >> pos.y;
		created.push_back(std::pair<std::string, sf::Vector2f>(name, pos));

	}

	packet >> leftClick;
	packet >> num;
	packet >> pos.x;
	packet >> pos.y;

	for(unsigned int i = 0; i < num; i++)
	{
		std::string id = "";
		packet >> id;
		GameObject* object = engine.getObject(id);
		if(object != NULL)
			objs.push_back(object);

	}

}

void Command::addTo(sf::Packet& packet)
{
	packet << team;
	packet << (unsigned int)created.size();
	for(unsigned int i = 0; i < created.size(); i++)
	{
		packet << created[i].first << created[i].second.x << created[i].second.y;

	}

	packet << leftClick;
	packet << (unsigned int)objs.size();
	packet << pos.x << pos.y;
	for(unsigned int i = 0; i < objs.size(); i++)
	{
		packet << objs[i]->getIDString();

	}

}

void Command::execute()
{
	Engine& engine = Engine::get();
	for(unsigned int i = 0; i < created.size(); i++)
	{
		std::string name = created[i].first;
		sf::Vector2f pos = created[i].second;
		std::string id;
		if(name == "archer")
			id = engine.addObject(new Archer(team), pos);
		else if(name == "archery range")
			id = engine.addObject(new ArcheryRange(team), pos);
		else if(name == "barracks")
			id = engine.addObject(new Barracks(team), pos);
		else if(name == "knight")
			id = engine.addObject(new Knight(team), pos);
		else if(name.find("scafold") != std::string::npos)
		{
			std::string building = name.substr(std::string("scafold").length());
			int bsizex = 1;
			int bsizey = 1;
			int bwork = 0;
			if(building == "stockpile")
			{
				bsizex = 1;
				bsizey = 1;
				bwork = 30*10;

			}
			else if(building == "archery range")
			{
				bsizex = 3;
				bsizey = 1;
				bwork = 30*30;

			}
			else if(building == "barracks")
			{
				bsizex = 3;
				bsizey = 1;
				bwork = 30*30;

			}

			id = engine.addObject(new Scafold(team, bsizex, bsizey, bwork, building), pos);

		}
		else if(name == "stockpile")
			id = engine.addObject(new Stockpile(team), pos);
		else if(name == "worker")
			id = engine.addObject(new Worker(team), pos);

		std::cout << name << " network id: " << id << "\n";

	}

	GameObject* obj = engine.getObject(pos);
	for(auto object : objs)
	{
		if(object->team == team)
		{
			object->pickedstate = false;
			object->onMessage(leftClick, obj);
			if(!object->pickedstate)
			{
				object->onMessage(leftClick, pos);
				if(!object->pickedstate)
				{
					object->onMessage(leftClick);

				}

			}

		}

	}

}
