#include "Button.h"
#include "Engine.h"

Button::Button(std::string namestr, sf::Vector2f size,
		sf::Texture* text, std::string hoverstr,
		sf::Font& font, sf::Vector2f pos)
{
	button.setFillColor(sf::Color(100, 100, 100, 100));
	button.setOutlineColor(sf::Color::Black);
	button.setOutlineThickness(1.0f);
	button.setSize(size);
	button.setOrigin(size.x/2, size.y/2);
	button.setPosition(pos);

	name = sf::Text(namestr, font, 16);
	name.setOrigin(name.getGlobalBounds().width/2, name.getGlobalBounds().height/2);
	name.setPosition(button.getPosition());

	hover = sf::Text(hoverstr, font, 14);
	sf::FloatRect hoverBounds = hover.getGlobalBounds();
	hoverBox.setSize(sf::Vector2f(hoverBounds.width + 10, hoverBounds.height + 10));
	hoverBox.setFillColor(sf::Color(150, 150, 150, 240));
	hoverBox.setOrigin(hoverBox.getSize().x/2, hoverBox.getSize().y/2);
	hoverBox.setPosition(button.getPosition());
	hoverBox.move(0, -button.getSize().y/2);
	hover.setPosition(hoverBox.getPosition());
	hover.move(-hoverBox.getSize().x/2, -hoverBox.getSize().y/2);

	ingame = false;

	if(text != NULL)
	{
		sf::Vector2u s = text->getSize();
		icon.setSize(sf::Vector2f(s.x, s.y));
		icon.setOrigin(s.x/2, s.y/2);
		icon.setTexture(text);
		icon.setPosition(button.getPosition());

	}
	else
	{
		icon.setFillColor(sf::Color::Transparent);

	}

}

bool Button::update(sf::RenderWindow& window, sf::RenderStates states)
{
	sf::Vector2f mousePos;
	if(!ingame)
	{
		sf::Vector2i mouse = sf::Mouse::getPosition(window);
		mousePos = sf::Vector2f(mouse.x, mouse.y);

	}
	else
	{
		mousePos = Engine::get().mousePos;

	}

	window.draw(button);
	window.draw(icon);
	window.draw(name);

	bool hovered = false;

	if(button.getGlobalBounds().contains(mousePos))
	{
		hovered = true;
		button.setFillColor(sf::Color(255, 255, 0, 100));
		window.draw(button);
		button.setFillColor(sf::Color(100, 100, 100, 100));
		window.draw(hoverBox);
		window.draw(hover);

	}

	return hovered && sf::Mouse::isButtonPressed(sf::Mouse::Left);

}

void Button::setPosition(sf::Vector2f pos)
{
	button.setPosition(pos);
	name.setPosition(button.getPosition());
	hoverBox.setPosition(button.getPosition());
	hover.setPosition(hoverBox.getPosition());
	hover.move(-hoverBox.getSize().x/2, -hoverBox.getSize().y/2);
	icon.setPosition(button.getPosition());

}
