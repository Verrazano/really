#include "Barracks.h"
#include "../Engine.h"
#include "Knight.h"

sf::Texture Barracks::barracksText;

Barracks::Barracks(int team) :
	Building(30*10, 100, 25, 0,
			&Engine::get().archerIcon,
			Engine::get().m_font, "Footmen",
			"25x wood, 25x gold, 100x stone\ndmg: 20\nhp: 100",
			team, sf::Vector2f(96, 32))
{
	static bool loaded =false;
	if(!loaded)
	{
		loaded =true;
		barracksText.loadFromFile("res/barracks.png");

	}

	sprite.setTexture(&barracksText);
	sprite.setSize(sf::Vector2f(96, 64));
	sprite.setTextureRect(sf::IntRect(0, 64*team, 96, 64));
	sprite.setOrigin(sprite.getOrigin().x, 48);

}

std::string Barracks::getName()
{
	return "barraks";

}

void Barracks::onProduce()
{
	sf::Vector2f pos = rect.getPosition();
	pos.y += rect.getSize().y/2 + 10;

	Engine::get().addNetworkObject("knight", pos);

}
