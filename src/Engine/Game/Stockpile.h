#ifndef STOCKPILE_H_
#define STOCKPILE_H_

#include <SFML/Graphics.hpp>
#include "../GameObject.h"

class Stockpile : public GameObject
{
public:
	Stockpile(int team);

	std::string getName();

	std::string type;

};

#endif /* STOCKPILE_H_ */
