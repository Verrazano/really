#include "ArcheryRange.h"
#include "../Engine.h"
#include "Archer.h"

sf::Texture ArcheryRange::archeryText;

ArcheryRange::ArcheryRange(int team) :
	Building(30*10, 100, 25, 0,
			&Engine::get().archerIcon,
			Engine::get().m_font, "Archer",
			"100x wood, 25x goold\ndmg: 15\nhp: 100",
			team, sf::Vector2f(96, 32))
{
	static bool loaded =false;
	if(!loaded)
	{
		loaded =true;
		archeryText.loadFromFile("res/archeryrange.png");

	}

	sprite.setTexture(&archeryText);
	sprite.setSize(sf::Vector2f(96, 64));
	sprite.setTextureRect(sf::IntRect(0, 64*team, 96, 64));
	sprite.setOrigin(sprite.getOrigin().x, 48);

}

std::string ArcheryRange::getName()
{
	return "archery range";

}

void ArcheryRange::onProduce()
{
	sf::Vector2f pos = rect.getPosition();
	pos.y += rect.getSize().y/2 + 10;

	Engine::get().addNetworkObject("archer", pos);

}
