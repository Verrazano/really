#ifndef GOLD_H_
#define GOLD_H_

#include "../GameObject.h"
#include <SFML/Audio.hpp>

class Gold : public GameObject
{
public:
	Gold();

	std::string getName();

	void onDeath();

	static sf::Texture goldText;
	static sf::SoundBuffer goldDieBuffer;
	static sf::Sound goldDie;

};

#endif /* GOLD_H_ */
