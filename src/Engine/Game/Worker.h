#ifndef WORKER_H_
#define WORKER_H_

#include "Unit.h"
#include <SFML/Audio.hpp>

class Worker : public Unit
{
public:
		Worker(int team);
		std::string getName();

		void onSelected();
		void onMessage(bool leftClick, sf::Vector2f pos);
		void onMessage(bool leftClick, GameObject* object);

		void onTick();

		static sf::SoundBuffer chopBuffer;
		sf::Sound chop;

		static sf::SoundBuffer mineBuffer;
		sf::Sound mine;

		static sf::Texture workerText;

		const int gatherTime = 5*30;
		int gatherTimer;
		std::string gatherId;
		int gathering;
		State backup;
		int gatheranim;

};

#endif /* WORKER_H_ */
