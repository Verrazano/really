#ifndef SCAFOLD_H_
#define SCAFOLD_H_

#include "../GameObject.h"

class Scafold : public GameObject
{
public:
	Scafold(int team,
			int bsizex, int bsizey, int work, std::string name);

	std::string getName();

	void onTick();

	std::string building;
	int work;

};

#endif /* SCAFOLD_H_ */
