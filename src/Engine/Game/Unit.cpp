#include "Unit.h"
#include "../Engine.h"
#include <iostream>

Unit::Unit(int team) :
	GameObject(team, sf::Vector2f(20, 20))
{
	sprite.setSize(sf::Vector2f(32, 32));
	sprite.setOrigin(16, 16);
	speed = 5;
	state = Nothing;
	/*if(team == 0)
		sprite.setFillColor(sf::Color(0, 0, 255, 150));
	else if(team == 1)
		sprite.setFillColor(sf::Color(255, 0, 0, 150));*/

	index = 0;
	repath = 0;

}

std::string Unit::getName()
{
	return "unit";

}

void Unit::onMessage(bool leftClick, sf::Vector2f pos)
{
	if(!leftClick)
	{
		pickedstate = true;
		state = Moving;
		setMoveTarget(pos);

	}

}

void Unit::setMoveTarget(sf::Vector2f pos)
{
	repath = 0;
	path = Engine::get().solvePath(rect.getPosition(), pos);
	if(path.size() == 0)
	{
		moveTarget = pos;
		state = Nothing;

	}
	else
	{
		moveTarget = path[0];

	}
	index = 0;

}

void Unit::onDraw(sf::RenderTarget& target, sf::RenderStates states)
{
	Engine& engine = Engine::get();
	if(state == Moving && engine.getDebug())
	{
		engine.drawPath(path);

	}

}

void Unit::onTick()
{
	if(state == Moving)
	{
		if((index+1 != path.size() && distanceTo(moveTarget) <= 12) ||
				(index+1 == path.size() && distanceTo(moveTarget) <= 36))
		{
			if(index+1 == path.size())
			{
				state = Nothing;

			}
			index++;
			moveTarget = path[index];
			repath = 0;
			return;

		}

		sf::Vector2f m;
		sf::Vector2f pos = rect.getPosition();
		if(!(fabs(moveTarget.x-pos.x) <= 2))
		{
			if(moveTarget.x > pos.x )
				m.x += speed;
			else
				m.x -= speed;

		}

		if(!(fabs(moveTarget.y-pos.y) <= 2))
		{
			if(moveTarget.y > pos.y)
				m.y += speed;
			else
				m.y -= speed;

		}

		if((m.x+m.y) >= speed*2)
		{
			m.x /= 2;
			m.y /= 2;

		}

		if(repath >= 15)
		{
			setMoveTarget(path[path.size()-1]);

		}
		repath++;

		rect.move(m);

	}

}

