#ifndef STONE_H_
#define STONE_H_

#include "../GameObject.h"
#include <SFML/Audio.hpp>

class Stone : public GameObject
{
public:
	Stone();

	std::string getName();

	void onDeath();

	static sf::Texture stoneText;
	static sf::SoundBuffer stoneDieBuffer;
	static sf::Sound stoneDie;

};

#endif /* STONE_H_ */
