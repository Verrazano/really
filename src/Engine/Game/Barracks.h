#ifndef BARRACKS_H_
#define BARRACKS_H_

#include "Building.h"

class Barracks : public Building
{
public:
	Barracks(int team);

	std::string getName();
	void onProduce();

	static sf::Texture barracksText;

};


#endif /* BARRACKS_H_ */
