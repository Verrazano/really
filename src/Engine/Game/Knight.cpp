#include "Knight.h"
#include "../Engine.h"

sf::Texture Knight::knightText;

Knight::Knight(int team) :
		Unit(team)
{
	searchTick = 0;
	backup = Nothing;
	attackAnim = 0;
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		knightText.loadFromFile("res/knight.png");

	}

	sprite.setTexture(&knightText);
	sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));

}

std::string Knight::getName()
{
	return "knight";

}

void Knight::onMessage(bool leftClick, sf::Vector2f pos)
{
	Unit::onMessage(leftClick, pos);
	if(pickedstate)
	{
		backup = Nothing;
		attackId = "";
		searchTick = 0;
		sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));

	}

}

void Knight::onMessage(bool leftClick, GameObject* object)
{
	if(!leftClick && object != NULL)
	{
		if(object->team != -1 && object->team != team)
		{
			pickedstate = true;
			backup = Attacking;
			state = Moving;
			attackId = object->getIDString();
			setMoveTarget(object->rect.getPosition());
			attackAnim = 0;
			searchTick = 0;

		}

	}

}

void Knight::onTick()
{
	Engine& engine = Engine::get();
	if(state == Nothing)
	{
		searchTick++;
		if(searchTick >= 15)
		{
			GameObject* obj = engine.getClosestObjectInRadiusNotOnTeam(32*6, rect.getPosition(), team);
			if(obj != NULL)
			{
				backup = Attacking;
				state = Moving;
				attackId = obj->getIDString();
				setMoveTarget(obj->rect.getPosition());
				attackAnim = 0;
				searchTick = 0;

			}
			searchTick = 0;

		}

	}
	else if(state == Moving)
	{
		Unit::onTick();
		if(backup == Attacking)
		{
			GameObject* obj = engine.getObject(attackId);
			if(obj == NULL)
			{
				sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));
				state = Nothing;
				backup = Nothing;
				attackId = "";
				searchTick = 0;
				return;

			}

			if(distanceTo(obj->rect.getPosition()) <= 32)
			{
				state = Attacking;
				attackAnim = 0;

			}

		}

	}
	else if(state == Attacking)
	{
		GameObject* obj = engine.getObject(attackId);
		if(obj == NULL)
		{
			state = Nothing;
			backup = Nothing;
			attackId = "";
			searchTick = 0;
			return;

		}

		if(!(distanceTo(obj->rect.getPosition()) <= 32))
		{
			sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));
			backup = Attacking;
			state = Moving;
			setMoveTarget(obj->rect.getPosition());

		}

		attackAnim++;
		if(attackAnim >= 30)
		{
			attackAnim = 0;
			obj->health -= 20;
			sprite.setTextureRect(sf::IntRect(32, team*32, 32, 32));

		}
		else if(attackAnim >= 25)
		{
			sprite.setTextureRect(sf::IntRect(32*2, team*32, 32, 32));

		}
		else if(attackAnim >= 15)
		{
			sprite.setTextureRect(sf::IntRect(32, team*32, 32, 32));

		}

	}

}

