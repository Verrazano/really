#ifndef ARCHER_H_
#define ARCHER_H_

#include "Unit.h"
#include <SFML/Audio.hpp>

class Archer : public Unit
{
public:
	Archer(int team);

	std::string getName();

	void onMessage(bool leftClick, sf::Vector2f pos);
	void onMessage(bool leftClick, GameObject* object);

	void onTick();

	static sf::Texture archerText;
	std::string attackId;
	State backup;
	int shootAnim;
	int searchTick;

	static sf::SoundBuffer bowshootBuffer;
	sf::Sound bowshoot;

};

#endif /* ARCHER_H_ */
