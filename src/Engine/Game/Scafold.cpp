#include "Scafold.h"
#include "../Engine.h"
#include "Stockpile.h"
#include "ArcheryRange.h"
#include "Barracks.h"

Scafold::Scafold(int team, int bsizex, int bsizey, int work, std::string name) :
	GameObject(team, sf::Vector2f(bsizex*32, bsizey*32))
{
	this->work = work;
	this->building = name;

}

std::string Scafold::getName()
{
	return "scafold";

}

void Scafold::onTick()
{
	if(work <= 0)
	{
		health = -1;
		if(building == "stockpile")
		{
			Engine::get().addObject(new Stockpile(team), rect.getPosition());

		}
		else if(building == "archery range")
		{
			Engine::get().addObject(new ArcheryRange(team), rect.getPosition());

		}
		else if(building == "barracks")
		{
			Engine::get().addObject(new Barracks(team), rect.getPosition());

		}

	}

}
