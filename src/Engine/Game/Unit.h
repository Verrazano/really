#ifndef UNIT_H_
#define UNIT_H_

#include "../GameObject.h"

class Unit : public GameObject
{
public:
	typedef enum State
	{
		Moving = 0,
		Attacking,
		GatheringWood,
		GatheringGold,
		GatheringStone,
		Patrolling,
		Nothing,
		Building

	} State;

	Unit(int team);

	std::string getName();

	void onMessage(bool leftClick, sf::Vector2f pos);

	void onTick();

	void onDraw(sf::RenderTarget& target, sf::RenderStates states = sf::RenderStates::Default);

	void setMoveTarget(sf::Vector2f pos);

	float speed;
	sf::Vector2f moveTarget;
	State state;
	std::vector<sf::Vector2f> path;
	int index;
	int repath;

};

#endif /* UNIT_H_ */
