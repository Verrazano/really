#ifndef BUILDING_H_
#define BUILDING_H_

#include "../GameObject.h"
#include "../Button.h"
#include <SFML/Audio.hpp>

class Building : public GameObject
{
public:
	Building(int time, int wood, int gold, int stone,
			sf::Texture* icon, sf::Font& font,
			std::string name, std::string hover,
			int team, sf::Vector2f size);

	virtual std::string getName() = 0;

	void onDraw(sf::RenderTarget& window, sf::RenderStates states = sf::RenderStates::Default);

	void onTick();

	virtual void onProduce() = 0;

	int wood;
	int gold;
	int stone;
	int timetobuild;
	int time;
	int queued;
	int stock;

	bool positionunset;

	static sf::SoundBuffer sheehaBuffer;
	static sf::Sound sheeha;

	Button b;

};

#endif /* BUILDING_H_ */
