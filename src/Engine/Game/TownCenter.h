#ifndef TOWNCENTER_H_
#define TOWNCENTER_H_

#include "Building.h"

class TownCenter : public Building
{
public:
	TownCenter(int team);

	std::string getName();

	void onProduce();

	static sf::Texture tcText;

};

#endif /* TOWNCENTER_H_ */
