#include "Archer.h"
#include "../Engine.h"

sf::Texture Archer::archerText;
sf::SoundBuffer Archer::bowshootBuffer;

Archer::Archer(int team) :
		Unit(team)
{
	searchTick = 0;
	backup = Nothing;
	shootAnim = 0;
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		archerText.loadFromFile("res/archer.png");
		bowshootBuffer.loadFromFile("res/bowshoot.ogg");

	}

	bowshoot.setBuffer(bowshootBuffer);
	sprite.setTexture(&archerText);
	sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));

}

std::string Archer::getName()
{
	return "archer";

}

void Archer::onMessage(bool leftClick, sf::Vector2f pos)
{
	Unit::onMessage(leftClick, pos);
	if(pickedstate)
	{
		backup = Nothing;
		attackId = "";
		searchTick = 0;
		sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));

	}

}

void Archer::onMessage(bool leftClick, GameObject* object)
{
	if(!leftClick && object != NULL)
	{
		if(object->team != -1 && object->team != team)
		{
			pickedstate = true;
			backup = Attacking;
			state = Moving;
			attackId = object->getIDString();
			setMoveTarget(object->rect.getPosition());
			shootAnim = 0;
			searchTick = 0;

		}

	}

}

void Archer::onTick()
{
	Engine& engine = Engine::get();
	if(state == Nothing)
	{
		searchTick++;
		if(searchTick >= 15)
		{
			GameObject* obj = engine.getClosestObjectInRadiusNotOnTeam(32*6, rect.getPosition(), team);
			if(obj != NULL)
			{
				backup = Attacking;
				state = Moving;
				attackId = obj->getIDString();
				setMoveTarget(obj->rect.getPosition());
				shootAnim = 0;
				searchTick = 0;

			}
			searchTick = 0;

		}

	}
	else if(state == Moving)
	{
		Unit::onTick();
		if(backup == Attacking)
		{
			GameObject* obj = engine.getObject(attackId);
			if(obj == NULL)
			{
				sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));
				state = Nothing;
				backup = Nothing;
				attackId = "";
				searchTick = 0;
				return;

			}

			if(distanceTo(obj->rect.getPosition()) <= 32*4)
			{
				state = Attacking;
				shootAnim = 0;

			}

		}

	}
	else if(state == Attacking)
	{
		GameObject* obj = engine.getObject(attackId);
		if(obj == NULL)
		{
			state = Nothing;
			backup = Nothing;
			attackId = "";
			searchTick = 0;
			return;

		}

		if(!(distanceTo(obj->rect.getPosition()) <= 32*4))
		{
			sprite.setTextureRect(sf::IntRect(0, team*32, 32, 32));
			backup = Attacking;
			state = Moving;
			setMoveTarget(obj->rect.getPosition());

		}

		shootAnim++;
		if(shootAnim >= 30)
		{
			shootAnim = 0;
			obj->health -= 15;
			sprite.setTextureRect(sf::IntRect(32, team*32, 32, 32));
			engine.playSound(bowshoot, rect.getPosition());

		}
		else if(shootAnim >= 25)
		{
			sprite.setTextureRect(sf::IntRect(32*3, team*32, 32, 32));

		}
		else if(shootAnim >= 15)
		{
			sprite.setTextureRect(sf::IntRect(32*2, team*32, 32, 32));

		}
		else if(shootAnim >= 5)
		{
			sprite.setTextureRect(sf::IntRect(32, team*32, 32, 32));

		}

	}

}
