#include "Gold.h"
#include "../Engine.h"

sf::Texture Gold::goldText;
sf::SoundBuffer Gold::goldDieBuffer;
sf::Sound Gold::goldDie;

Gold::Gold() :
		GameObject(-1, sf::Vector2f(32, 32))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		goldText.loadFromFile("res/golds.png");
		goldDieBuffer.loadFromFile("res/golddie.ogg");
		goldDie.setBuffer(goldDieBuffer);

	}

	isStatic = true;

	sprite.setTexture(&goldText);
	int r = rand()%2;
	if(r == 0)
		r--;
	sprite.setScale(r, 1);
	int d = rand()%3;
	sprite.setTextureRect(sf::IntRect(32*d, 0, 32, 32));

}

std::string Gold::getName()
{
	return "gold";

}

void Gold::onDeath()
{
	Engine::get().playSound(goldDie, rect.getPosition());

}
