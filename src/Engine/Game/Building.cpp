#include "Building.h"
#include "../Engine.h"

sf::SoundBuffer Building::sheehaBuffer;
sf::Sound Building::sheeha;

Building::Building(int time, int wood, int gold, int stone,
		sf::Texture* icon, sf::Font& font,
		std::string name, std::string hover,
		int team, sf::Vector2f size) :
		GameObject(team, size)
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		sheehaBuffer.loadFromFile("res/sheeha.wav");
		sheeha.setBuffer(sheehaBuffer);

	}

	timetobuild = time;
	this->time = 0;
	this->wood = wood;
	this->gold = gold;
	this->stone = stone;
	b = Button(name, sf::Vector2f(32, 32),
			icon, hover, font, sf::Vector2f(0, 0));
	b.ingame = true;
	positionunset = true;
	queued = 0;
	stock = -1;//infinite (as long as you have the material)
	isStatic = true;
}

void Building::onDraw(sf::RenderTarget& window,
		sf::RenderStates states)
{
	if(selected)
	{
		Engine& engine = Engine::get();
		sf::RenderWindow& win = engine.m_window;
		if(b.update(win))
		{
			if(queued < 5 && (stock == -1 || stock > 0) && engine.ggold >= gold &&
					engine.gstone >= stone &&
					engine.gwood >= wood)
			{
				if(stock != -1)
					stock--;

				engine.gstone -= stone;
				engine.ggold -= gold;
				engine.gwood -= wood;

				queued++;

			}

		}

	}

}

void Building::onTick()
{
	if(positionunset)
	{
		positionunset = false;
		b.setPosition(rect.getPosition());

	}

	if(queued > 0)
	{
		time++;
		if(time >= timetobuild)
		{
			queued--;
			time = 0;
			onProduce();
			Engine::get().playSound(sheeha, rect.getPosition());

		}

	}

}
