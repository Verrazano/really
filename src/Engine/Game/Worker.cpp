#include "Worker.h"
#include "../Engine.h"
#include <iostream>
#include "Scafold.h"

sf::SoundBuffer Worker::chopBuffer;
sf::SoundBuffer Worker::mineBuffer;
sf::Texture Worker::workerText;

Worker::Worker(int team) :
	Unit(team)
{
	backup = Nothing;
	gathering = 0;
	gatherTimer = 0;
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		chopBuffer.loadFromFile("res/chop.wav");
		mineBuffer.loadFromFile("res/mine.ogg");
		workerText.loadFromFile("res/worker.png");

	}

	sprite.setTexture(&workerText);
	sprite.setTextureRect(sf::IntRect(32*3, 32*team, 32, 32));

	chop.setBuffer(chopBuffer);
	chop.setVolume(50);

	mine.setBuffer(mineBuffer);
	gatheranim = 0;

}

std::string Worker::getName()
{
	return "worker";

}

void Worker::onSelected()
{
	Engine::get().showWorkerMenu();

}

void Worker::onMessage(bool leftClick, sf::Vector2f pos)
{
	Unit::onMessage(leftClick, pos);
	if(pickedstate)
	{
		sprite.setTextureRect(sf::IntRect(32*3, 32*team, 32, 32));
		gathering = 0;
		gatherTimer = 0;
		backup = Nothing;

	}

}

void Worker::onMessage(bool leftClick, GameObject* object)
{
	if(!leftClick && object != NULL)
	{
		if(object->getName() == "scafold")
		{
			pickedstate = true;
			std::cout << "go build\n";
			backup = Building;
			state = Moving;
			setMoveTarget(object->rect.getPosition());
			gatherId = object->getIDString();
			gathering = 0;
			sprite.setTextureRect(sf::IntRect(32*2, 32*team, 32, 32));
			gatheranim = 0;

		}
		else if(object->getName() == "tree")
		{
			pickedstate = true;
			state = Moving;
			gatherId = object->getIDString();
			gathering = 1;
			setMoveTarget(object->rect.getPosition());
			backup = GatheringWood;
			sprite.setTextureRect(sf::IntRect(32*1, 32*team, 32, 32));
			gatheranim = 0;
			gatherTimer = 0;

		}
		else if(object->getName() == "stone")
		{
			pickedstate = true;
			state = Moving;
			gatherId = object->getIDString();
			gathering = 1;
			setMoveTarget(object->rect.getPosition());
			backup = GatheringStone;
			sprite.setTextureRect(sf::IntRect(0, 32*team, 32, 32));
			gatheranim = 0;
			gatherTimer = 0;

		}
		else if(object->getName() == "gold")
		{
			pickedstate = true;
			state = Moving;
			gatherId = object->getIDString();
			gathering = 1;
			setMoveTarget(object->rect.getPosition());
			backup = GatheringGold;
			sprite.setTextureRect(sf::IntRect(0, 32*team, 32, 32));
			gatheranim = 0;
			gatherTimer = 0;

		}
		else if(!leftClick && object->getName() == "stockpile")
		{
			pickedstate = true;
			gathering = 2;
			state = Moving;
			backup = GatheringWood;
			setMoveTarget(object->rect.getPosition());
			sprite.setTextureRect(sf::IntRect(32, 32*team, 32, 32));
			gatheranim = 0;

		}

	}

}

void Worker::onTick()
{
	if(sprite.getRotation() != 0)
	{
		sprite.rotate(-2);

	}

	if(state == Moving)
	{
		chop.stop();
		mine.stop();
		Unit::onTick();
		if(state == Nothing && backup == Building)
		{
			std::cout << "switch to building\n";
			state = Building;
			gathering = 0;
			gatheranim = 0;
			sprite.setTextureRect(sf::IntRect(32*2, 32*team, 32, 32));

		}
		else if(state == Nothing && gathering == 1)
		{
			gathering = 2;
			state = backup;
			gatheranim = 0;
			gatherTimer = 0;

		}
		else if(state == Nothing && gathering == 2)
		{
			Engine& engine = Engine::get();
			if(team == engine.getTeam())
			{
				if(backup == GatheringWood)
				{
					engine.gwood += 25;

				}
				else if(backup == GatheringStone)
				{
					engine.gstone += 25;

				}
				else if(backup == GatheringGold)
				{
					engine.ggold += 25;

				}

			}

			GameObject* gatherme = engine.getObject(gatherId);
			if(gatherme == NULL)
			{
				std::string objstr = "";
				if(backup == GatheringWood)
				{
					objstr = "tree";

				}
				else if(backup == GatheringStone)
				{
					objstr = "stone";

				}
				else if(backup == GatheringGold)
				{
					objstr = "gold";

				}

				GameObject* obj = engine.getClosestObjectWithName(objstr, rect.getPosition());
				if(obj == NULL)
				{
					state = Nothing;
					backup = Nothing;
					gathering = 0;
					gatherTimer = 0;
					return;

				}

				gatherme = obj;
				gatherId = obj->getIDString();

			}
			state = Moving;
			gathering = 1;
			setMoveTarget(gatherme->rect.getPosition());

		}

	}
	else if(state == GatheringWood || state == GatheringStone || state == GatheringGold)
	{
		if(state == GatheringWood && chop.getStatus() != sf::Sound::Playing)
		{
			Engine::get().playSound(chop, rect.getPosition(), 50);

		}
		else if((state == GatheringStone || state == GatheringGold) && mine.getStatus() != sf::Sound::Playing)
		{
			Engine::get().playSound(mine, rect.getPosition());

		}

		gatheranim++;
		if(gatheranim >= 25)
		{
			sprite.setRotation(20);
			gatheranim = 0;

		}

		if(gatherTimer >= gatherTime)
		{
			chop.stop();
			mine.stop();
			backup = state;
			state = Moving;
			gatherTimer = 0;

			Engine& engine = Engine::get();
			GameObject* stock = engine.getClosestObjectWithNameOnTeam("stockpile", rect.getPosition(), team);
			if(stock == NULL)
			{
				state = Nothing;
				backup = Nothing;
				gathering = 0;
				return;

			}

			setMoveTarget(stock->rect.getPosition());

			GameObject* gatherme = engine.getObject(gatherId);
			if(gatherme == NULL)
			{
				return;

			}
			gatherme->health -= 25;

		}
		gatherTimer++;

	}
	else if(state == Building)
	{
		gatheranim++;
		if(gatheranim >= 25)
		{
			sprite.setRotation(20);
			gatheranim = 0;
			gatherTimer = 0;

		}

		Engine& engine = Engine::get();
		Scafold* scaf = (Scafold*)engine.getObject(gatherId);
		if(scaf == NULL)
		{
			state = Nothing;
			backup = Nothing;
			gathering = 0;
			return;

		}
		scaf->work--;

	}

}
