#include "Stone.h"
#include "../Engine.h"

sf::Texture Stone::stoneText;
sf::SoundBuffer Stone::stoneDieBuffer;
sf::Sound Stone::stoneDie;

Stone::Stone() :
		GameObject(-1, sf::Vector2f(32, 32))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		stoneText.loadFromFile("res/stones.png");
		stoneDieBuffer.loadFromFile("res/stonedie.ogg");
		stoneDie.setBuffer(stoneDieBuffer);

	}

	isStatic = true;

	sprite.setTexture(&stoneText);
	int r = rand()%2;
	if(r == 0)
		r--;
	sprite.setScale(r, 1);
	int d = rand()%3;
	sprite.setTextureRect(sf::IntRect(32*d, 0, 32, 32));

}

std::string Stone::getName()
{
	return "stone";

}

void Stone::onDeath()
{
	Engine::get().playSound(stoneDie, rect.getPosition());

}
