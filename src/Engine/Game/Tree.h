#ifndef TREE_H_
#define TREE_H_

#include "../GameObject.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class Tree : public GameObject
{
public:
	Tree();

	std::string getName();

	bool draw(const sf::FloatRect& view, sf::RenderTarget& window, sf::RenderStates states = sf::RenderStates::Default);

	void onDeath();

	float offset;

	static sf::Texture treeText;

	static sf::Clock waveTimer;
	static sf::Shader wave;

	static sf::SoundBuffer treeDieBuffer;
	static sf::Sound treeDie;

	void updateShader(float f, float x, float y, float t);

};

#endif /* TREE_H_ */
