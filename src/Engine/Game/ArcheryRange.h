#ifndef ARCHERYRANGE_H_
#define ARCHERYRANGE_H_

#include "Building.h"

class ArcheryRange : public Building
{
public:
	ArcheryRange(int team);

	std::string getName();
	void onProduce();

	static sf::Texture archeryText;

};

#endif /* ARCHERYRANGE_H_ */
