#include "Tree.h"
#include <iostream>
#include "../Engine.h"

sf::Texture Tree::treeText;
sf::Shader Tree::wave;
sf::Clock Tree::waveTimer;
sf::SoundBuffer Tree::treeDieBuffer;
sf::Sound Tree::treeDie;

Tree::Tree() :
		GameObject(-1, sf::Vector2f(32, 48))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		treeText.loadFromFile("res/trees.png");
		wave.loadFromFile("res/wave.vert", "res/blur.frag");
		treeDieBuffer.loadFromFile("res/treedie.ogg");
		treeDie.setBuffer(treeDieBuffer);
		waveTimer.restart();

	}

	rect.setSize(sf::Vector2f(32, 32));
	rect.setOrigin(16, 8);
	sprite.setTexture(&treeText);
	int r = rand()%2;
	if(r == 0)
		r--;
	sprite.setScale(r, 1);
	int d = rand()%3;
	sprite.setTextureRect(sf::IntRect(32*d, 0, 32, 48));
	isStatic = true;

	offset = (rand()%100);

}

std::string Tree::getName()
{
	return "tree";

}

bool Tree::draw(const sf::FloatRect& view, sf::RenderTarget& window, sf::RenderStates states)
{
	states.shader = &wave;
	updateShader(offset, 0.001, 0.0001, waveTimer.getElapsedTime().asSeconds());
	return GameObject::draw(view, window, states);

}

void Tree::onDeath()
{
	Engine::get().playSound(treeDie, rect.getPosition());

}

void Tree::updateShader(float f, float x, float y, float t)
{
    wave.setParameter("wave_phase", t+f);
    wave.setParameter("wave_amplitude", x * 250, y * 40);
    wave.setParameter("blur_radius", (x + y) * 0.008f);

}
