#include "TownCenter.h"
#include "Worker.h"
#include "../Engine.h"

sf::Texture TownCenter::tcText;

TownCenter::TownCenter(int team) :
	Building(30*7, 50, 0, 25,
			&Engine::get().archerIcon,
			Engine::get().m_font, "Worker",
			"50x wood, 25x wood",
			team, sf::Vector2f(64, 32))
{
	static bool loaded = false;
	if(!loaded)
	{
		loaded = true;
		tcText.loadFromFile("res/tc.png");

	}

	sprite.setSize(sf::Vector2f(64, 96));
	sprite.setOrigin(32, 48+12);
	sprite.setTexture(&tcText);
	sprite.setTextureRect(sf::IntRect(team*64, 0, 64, 96));
	isStatic = true;

}

std::string TownCenter::getName()
{
	return "towncenter";

}

void TownCenter::onProduce()
{
	sf::Vector2f pos = rect.getPosition();
	pos.y += rect.getSize().y/2 + 10;

	Engine::get().addNetworkObject("worker", pos);

}
