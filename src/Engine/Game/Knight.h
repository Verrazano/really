#ifndef KNIGHT_H_
#define KNIGHT_H_

#include "Unit.h"

class Knight : public Unit
{
public:
	Knight(int team);

	std::string getName();

	void onMessage(bool leftClick, sf::Vector2f pos);
	void onMessage(bool leftClick, GameObject* object);

	void onTick();

	static sf::Texture knightText;
	std::string attackId;
	State backup;
	int attackAnim;
	int searchTick;

};

#endif /* KNIGHT_H_ */
