#include "Path.h"
#include <iostream>
#include "Engine.h"

int Path::MAPX;

Path::Path(bool dostuff)
{
	if(dostuff)
	{
		Engine& engine = Engine::get();
		std::vector<std::vector<int> > map = engine.getMap();
		MAPX = map[0].size();

	}

	gx =0;
	gy = 0;
	sx = 0;
	sy = 0;

}

void Path::nodeToXY(void* node, int& x, int& y )
{
	int index = (int)node;
	y = index / MAPX;
	x = index - y * MAPX;
}

void* Path::xyToNode(int x, int y)
{
	return (void*) ( y*MAPX + x );
}

float Path::LeastCostEstimate(void* stateStart, void* stateEnd)
{
	int xs, ys, xe, ye;
	nodeToXY(stateStart, xs, ys);
	nodeToXY(stateEnd, xe, ye);
	int dx = xs - xe;
	int dy = ys - ye;
	return (float)sqrtf((double)(dx*dx) + (double)(dy*dy));

}

void Path::AdjacentCost( void* state, std::vector< micropather::StateCost > *adjacent )
{
	int x, y;
	///					-, -, -,  -,  4,  5,  6,  7 }
	const int dx[8] = { 1, 0, -1,  0,  1, 1, -1, -1 };
	const int dy[8] = { 0, 1,  0, -1, -1, 1,  1, -1 };
	bool passable[8] = {false, false, false, false, false, false, false, false};
	const float costs[8] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.41f, 1.41f, 1.41f, 1.41f };

	nodeToXY(state, x, y );
	Engine& engine = Engine::get();
	std::vector<std::vector<int> > map = engine.getMap();

	for( int i=0; i<8; ++i )
	{
		int nx = x + dx[i];
		int ny = y + dy[i];

		GameObject* obj = engine.getObject(sf::Vector2f((nx*32)+16, (ny*32)+16));

		if(nx > -1 && nx < MAPX && ny > -1 &&
				ny < map.size())
		{

			if((nx == gx && gy == ny) || (nx == sx && sy == ny))
			{
				if(i > 3)
				{
					bool pass = true;
					if(i == 4 && (!passable[3] || !passable[0])) //upandright
						pass = false;
					else if(i == 5 && (!passable[1] || !passable[0]))//downandright
						pass = false;
					else if(i == 6 && (!passable[1] || !passable[2]))//downandleft
						pass = false;
					else if(i == 7 && (!passable[3] || !passable[2]))//upleft
						pass = false;

					if(!pass)
					{
						micropather::StateCost cost = {xyToNode(nx, ny), FLT_MAX};
						adjacent->push_back(cost);
						passable[i] = false;
						continue;

					}

				}

				micropather::StateCost cost = {xyToNode(nx, ny), costs[i]};
				adjacent->push_back(cost);
				passable[i] = true;

			}
			else if(map[ny][nx] == Engine::WATER || obj != NULL)
			{

				micropather::StateCost cost = {xyToNode(nx, ny), FLT_MAX};
				adjacent->push_back(cost);
				passable[i] = false;

			}
			else
			{
				if(i > 3)
				{
					bool pass = true;
					if(i == 4 && !passable[3] && !passable[0]) //upandright
						pass = false;
					else if(i == 5 && !passable[1] && !passable[0])//downandright
						pass = false;
					else if(i == 6 && !passable[1] && !passable[2])//downandleft
						pass = false;
					else if(i == 7 && !passable[3] && !passable[2])//upleft
						pass = false;

					if(!pass)
					{
						micropather::StateCost cost = {xyToNode(nx, ny), FLT_MAX};
						adjacent->push_back(cost);
						passable[i] = false;
						continue;

					}

				}

				micropather::StateCost cost = {xyToNode(nx, ny), costs[i]};
				adjacent->push_back(cost);
				passable[i] = true;

			}

		}
		else
		{
			micropather::StateCost cost = {xyToNode(nx, ny), FLT_MAX};
			adjacent->push_back(cost);
			passable[i] = false;

		}

	}

}

void  Path::PrintStateInfo( void* state )
{
	int x, y;
	nodeToXY(state, x, y );
	std::cout << x << ", " << y;

}
