#include "TileMap.h"
#include <iostream>
#include <math.h>

TileMap::TileMap(sf::Texture* texture, sf::Vector2f size) :
	m_texture(texture),
	m_size(size)
{
}

bool TileMap::load(std::string path)
{
	sf::Image img;
	if(!img.loadFromFile(path))
	{
		std::cout << "Error loading map: \"" << path << "\"\n";
		return false;

	}

	std::vector<std::vector<int> > map;

	int width = img.getSize().x;
	int height = img.getSize().y;

	for(int i = 0; i < height; i++)
	{
		map.push_back(std::vector<int>());
		for(int j = 0; j < width; j++)
		{
			sf::Color color = img.getPixel(j, i);
			int index = -1;
			for(auto pair : m_colorToIndex)
			{
				if(pair.first == color)
				{
					index = pair.second;
					break;

				}

			}

			map[i].push_back(index);

		}

	}

	load(map);

	return true;

}

void TileMap::load(std::vector<std::vector<int> > map)
{
	m_map = map;
	int height = map.size();
	int width = map[0].size();
	m_verts = sf::VertexArray(sf::Quads, width*height*4);
	for(int i = 0; i < height; i++)
	{
		for(int j = 0; j < width; j++)
		{
			sf::Vertex* vert = &m_verts[(i+j*width)*4];
			int index = map[i][j];
			int tx = 0;
			int ty = 0;
			bool useTexture  = false;

			if(m_texture != NULL && m_texture->getSize() != sf::Vector2u(0, 0))
			{
				useTexture = true;
				tx = index % (int)(m_texture->getSize().x / m_size.x);
				ty = index / (int)(m_texture->getSize().x / m_size.x);
				tx *= m_size.x;
				ty *= m_size.y;

			}

			sf::Color color = getColor(index);
			int x = j*m_size.x;
			int y = i*m_size.y;

			if(useTexture)
			{
				vert[0] = sf::Vertex(sf::Vector2f(x, y), sf::Vector2f(tx, ty));
				vert[1] = sf::Vertex(sf::Vector2f(x + m_size.x, y), sf::Vector2f(tx + m_size.x, ty));
				vert[2] = sf::Vertex(sf::Vector2f(x + m_size.x, y + m_size.y), sf::Vector2f(tx + m_size.x, ty + m_size.y));
				vert[3] = sf::Vertex(sf::Vector2f(x , y + m_size.y), sf::Vector2f(tx, ty + m_size.y));

			}
			else
			{
				vert[0] = sf::Vertex(sf::Vector2f(x, y), color);
				vert[1] = sf::Vertex(sf::Vector2f(x + m_size.x, y), color);
				vert[2] = sf::Vertex(sf::Vector2f(x + m_size.x, y + m_size.y), color);
				vert[3] = sf::Vertex(sf::Vector2f(x , y + m_size.y), color);

			}

		}

	}

}

void TileMap::draw(sf::RenderTarget& window, sf::RenderStates states)
{
	states.texture = m_texture;
	window.draw(m_verts, states);

}

int TileMap::getTileIndex(sf::Vector2f pos)
{
	pos.x /= m_size.x;
	pos.y /= m_size.y;
	pos.x = floorf(pos.x);
	pos.y = floorf(pos.y);
	return m_map[pos.y][pos.x];

}

void TileMap::setTileIndex(sf::Vector2f pos, int index)
{
	pos.x /= m_size.x;
	pos.y /= m_size.y;
	pos.x = floorf(pos.x);
	pos.y = floorf(pos.y);
	m_map[pos.y][pos.x] = index;
	sf::Color color = getColor(index);
	int x = pos.x*m_size.x;
	int y = pos.y*m_size.y;
	sf::Vertex* vert = &m_verts[(pos.x+pos.y)*4];
	vert[0] = sf::Vertex(sf::Vector2f(x, y), color, sf::Vector2f(x, y));
	vert[1] = sf::Vertex(sf::Vector2f(x + m_size.x, y), color, sf::Vector2f(x + m_size.x, y));
	vert[2] = sf::Vertex(sf::Vector2f(x + m_size.x, y + m_size.y), color, sf::Vector2f(x + m_size.x, y + m_size.y));
	vert[3] = sf::Vertex(sf::Vector2f(x , y + m_size.y), color, sf::Vector2f(x, y + m_size.y));

}

void TileMap::addColor(sf::Color color, int index)
{
	m_colorToIndex.push_back(std::pair<sf::Color, int>(color, index));

}

sf::Color TileMap::getColor(int index)
{
	for(auto pair : m_colorToIndex)
	{
		if(pair.second == index)
		{
			return pair.first;

		}

	}

	return sf::Color(255, 20, 147);

}

sf::Vector2f TileMap::getSize()
{
	return sf::Vector2f(m_map[0].size()*m_size.x, m_map.size()*m_size.y);

}

std::vector<std::vector<int> > TileMap::getMap()
{
	return m_map;

}
