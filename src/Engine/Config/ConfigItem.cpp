#include "Config.h"
#include <sstream>

ConfigItem::ConfigItem()
{
}

ConfigItem::ConfigItem(std::string key, std::string type, std::string line,
		std::string value, std::vector<std::string> params)
{
	m_key = key;
	m_type = type;
	m_line = line;
	m_value = value;
	m_params = params;

}

std::string ConfigItem::getKey()
{
	return m_key;

}

std::string ConfigItem::getType()
{
	return m_type;

}

std::string ConfigItem::getLine()
{
	return m_line;

}

std::string ConfigItem::getValue()
{
	return m_value;

}

std::vector<std::string> ConfigItem::getParams()
{
	return m_params;

}

unsigned int ConfigItem::getNumOfParams()
{
	return m_params.size();

}

std::string ConfigItem::getParam(unsigned int index)
{
	return m_params[index];

}

//TODO: replace these with template functions
int ConfigItem::valueAsInt()
{
	std::stringstream ss;
	ss << m_value;
	int i = 0;
	ss >> i;
	return i;

}

unsigned int ConfigItem::valueAsUInt()
{
	std::stringstream ss;
	ss << m_value;
	unsigned int i = 0;
	ss >> i;
	return i;

}

float ConfigItem::valueAsFloat()
{
	std::stringstream ss;
	ss << m_value;
	float i = 0;
	ss >> i;
	return i;

}

bool ConfigItem::valueAsBool()
{
	if(m_value == "true" || m_value == "True")
		return true;
	return false;

}

int ConfigItem::paramAsInt(unsigned int index)
{
	std::stringstream ss;
	ss << getParam(index);
	int i = 0;
	ss >> i;
	return i;

}

unsigned int ConfigItem::paramAsUInt(unsigned int index)
{
	std::stringstream ss;
	ss << getParam(index);
	unsigned int i = 0;
	ss >> i;
	return i;

}

float ConfigItem::paramAsFloat(unsigned int index)
{
	std::stringstream ss;
	ss << getParam(index);
	float i = 0;
	ss >> i;
	return i;

}

bool ConfigItem::paramAsBool(unsigned int index)
{
	std::string param = getParam(index);
	if(param == "true" || param == "True")
		return true;
	return false;

}



