#ifndef COMMAND_H_
#define COMMAND_H_

#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

class GameObject;

class Command
{
public:
	Command();
	Command(std::vector<GameObject*> objs, bool leftClick, sf::Vector2f pos);

	void recieve(sf::Packet& packet);
	void addTo(sf::Packet& packet);

	void execute();

	bool leftClick;
	std::vector<GameObject*> objs;
	sf::Vector2f pos;
	int team;
	std::vector<std::pair<std::string, sf::Vector2f> > created;

};

#endif /* COMMAND_H_ */
