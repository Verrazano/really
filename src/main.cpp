#include "Engine/Engine.h"

int main(int argc, char** argv)
{
	Engine& engine = Engine::get();
	if(engine.isHost)
	{
		engine.loadMap(engine.mapnamesettings);

	}
	engine.initNetwork();
	while(engine.isRunning())
	{
		engine.tick();

	}

	return 0;

}
